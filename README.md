# Test 12-2020


## Instruções para rodar o sistema
Steps:
- clone o repositório
- Cria um arquivo .env e adicione
  >API_KEY=...
  >
  >MARKETPLACE_RECIPIENT_ID=... # O RECIPIENT ID DO MARKETPLACE
  >
  >MARKETPLACE_PERCENTAGE=15 # O VALOR DE PORCENTAGEM QUE O MARKETPLACE RECEBERÁ
- python3 -m venv venv
- source venv/bin/activate
- pip install -r requirements.txt
- python manage.py migrate
- python manage.py loaddata fixtures/db.json
- python manage.py runserver
- acesse http://127.0.0.1:8000/admin/
  >username: admin
  >
  >password: admin123

## Instruções de uso da API

### TOKEN
Para adquirir o token acesse:

> (POST) http://127.0.0.1:8000/api/token-login/

**Body**
```json
{
  "username": "admin",
  "password": "admin123"
}
```
**Response**
```json
{
  "token": "a79ab24ba338423507a721b06f3a19dc8dd92e98"
}
```

Adicione o token no header dos requests
> Authorization: Token a79ab24ba338423507a721b06f3a19dc8dd92e98

### BAKERIES
> CREATE (POST)
>
> http://127.0.0.1:8000/api/sales/bakeries/

o **body** deverá ser semelhante a estrutura abaixo:
```json
{
  "user": {
    "password": "123456"
  },
  "fantasy_name": "Padaria Requinte",
  "document_number": "70.025.343/0001-96",
  "bank_account": {
    "agency": "0932",
    "bank_code": "341",
    "account": "58054",
    "account_dv": "1",
    "legal_name": "HOUSE TARGARYEN"
  }
}
```

> LIST (GET)
>
> http://127.0.0.1:8000/api/sales/bakeries/

**Response:**
```json
[
  {
    "id": 1,
    "fantasy_name": "Padaria Requinte",
    "document_number": "70025343000196",
    "items": []
  }
]
```

### ITEMS
> CREATE (POST)
>
> http://127.0.0.1:8000/api/sales/items/


o **body** deverá ser semelhante a estrutura abaixo:
```json
{
  "name": "Panetone",
  "price": "10.00",
  "stock": "10",
  "bakery": 1
}
```

> LIST (GET)
>
> http://127.0.0.1:8000/api/sales/items/
>
> http://127.0.0.1:8000/api/sales/items?search=panetone [**Search by name**]
>
> http://127.0.0.1:8000/api/sales/items?search=7 [**Search by price**]
>
>Obs.: Não precisa utilizar o token para listar os items


**Response:**
```json
[
  {
    "id": 1,
    "name": "Panetone",
    "price": "10.00",
    "stock": 10,
    "bakery": 4,
    "bakery__fantasy_name": "Padaria Requinte"
  }
]
```

### PAYMENTS
> CREATE (POST)
>
> http://127.0.0.1:8000/api/purchase/payments/


o **body** deverá ser semelhante a estrutura abaixo:
```json
{
  "card": {
    "number": "5551569088139916",
    "cvv": "369",
    "expiration_date": "0922",
    "holder_name": "Morpheus Fishburne"
  },
  "billing": {
    "name": "Trinity Moss",
    "country": "br",
    "state": "SE",
    "city": "Aracaju",
    "neighborhood": "Santo Antônio",
    "street": "Rua José Andrade Filho",
    "street_number": "123",
    "zipcode": "49060105"
  },
  "shipping": {
    "name": "Neo Reeves",
    "fee": "0",
    "delivery_date": "2000-12-31",
    "expedited": true,
    "country": "br",
    "state": "sp",
    "city": "Cotia",
    "neighborhood": "Rio Cotia",
    "street": "Rua Matrix",
    "street_number": "9999",
    "zipcode": "06714360"
  },
  "items": [
    {
      "id": 1,
      "qtd": "10"
    }
  ],
  "customer": {
    "name": "Morpheus Fishburne",
    "country": "br",
    "email": "mopheus@nabucodonozor.com",
    "type": "individual",
    "number": "00000000000",
    "type_doc": "cpf",
    "phone_numbers": ["+5511999998888"],
    "birthday": "1965-01-01"
  }
}
```

**Response:**
```json
{}
```

**OBS**
- Customer que não estão cadastrados podem fazer compras, basta enviar sem token
- e o customer criado no /admin para que sejam vinculado a uma compra deve ser enviado o token do customer.


### TESTS
>python manage.py test