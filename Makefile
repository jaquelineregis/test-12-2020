activate:
    # source "$( poetry env list --full-path | grep Activated | cut -d' ' -f1 )/bin/activate"
    source "$( poetry env list --full-path )/bin/activate"

run:
    python manage.py runserver

fixture:
    python manage.py dumpdata bakeries.Bakery --indent 4 > fixtures/bakery.json
    python manage.py dumpdata bakeries.Item --indent 4 > fixtures/item.json
