from django.core.exceptions import ValidationError
from django.db import models


class Order(models.Model):
    amount = models.DecimalField(
        "Amount", max_digits=15, decimal_places=2, blank=True, null=True
    )
    customer = models.ForeignKey(
        "customers.Customer", on_delete=models.CASCADE, null=True, blank=True
    )
    address_shipping = models.ForeignKey(
        "customers.AddressShipping", on_delete=models.CASCADE
    )
    payment_broker = models.CharField(max_length=255)
    payment_status = models.CharField(max_length=255)
    payment_id = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return f"{self.pk}"

    @property
    def ttl_amount(self):
        return sum([item.total for item in self.order_items.all()])


class OrderItem(models.Model):
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name="order_items"
    )
    item = models.ForeignKey("bakeries.Item", on_delete=models.CASCADE)
    qtd = models.PositiveIntegerField("Quantity")

    class Meta:
        unique_together = [("order", "item")]

    def __str__(self):
        return f"Item: {self.item} of Order: {self.order}"

    def save(self, *args, **kwargs):
        stock, qtd = self.item.stock, int(self.qtd)
        if stock < qtd:
            raise ValidationError(
                "%(item)s don't have %(qtd)s items just %(stock)s items in stock.",
                params={"item": self.item, "qtd": qtd, "stock": stock},
            )
        self.item.stock = stock - qtd
        self.item.save()
        super().save(*args, **kwargs)

    @property
    def total(self):
        return self.item.price * self.qtd

    @property
    def recipient_id(self):
        return self.item.bakery.recipients.recipient_id
