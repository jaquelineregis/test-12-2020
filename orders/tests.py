import json

from django.core.exceptions import ValidationError
from django.test import TestCase, Client

from model_bakery import baker
from rest_framework.authtoken.models import Token

from .models import OrderItem, Order
from bakeries.models import Item, BankAccount, Bakery


class UnitTest(TestCase):
    def test_when_save_order_item_change_stock_item(self):
        item = baker.make(Item, bakery=self.md_bakery(), stock=100)
        order_item = baker.make(OrderItem, item=item, qtd=10)
        self.assertEqual(item.stock, 90)

    def test_when_save_order_item_return_error_stock_less(self):
        item = baker.make(Item, bakery=self.md_bakery(), stock=10)
        with self.assertRaises(ValidationError) as context:
            baker.make(OrderItem, item=item, qtd=11)
        self.assertTrue(
            "don't have 11 items just 10 items in stock." in str(context.exception)
        )

    def test_when_call_ttl_amount_property_sum_correct(self):
        order = baker.make(Order)
        item1 = baker.make(Item, bakery=self.md_bakery(), stock=100, price=1.00)
        item2 = baker.make(
            Item, bakery=self.md_bakery("65283976009"), stock=100, price=2.00
        )
        order_item1 = baker.make(OrderItem, order=order, item=item1, qtd=10)
        order_item2 = baker.make(OrderItem, order=order, item=item2, qtd=10)
        self.assertEqual(order.ttl_amount, 30)

    def md_bakery(self, document_number="70025343000196"):
        bank_account = baker.make(
            BankAccount,
            agency="0932",
            bank_code="341",
            account="58054",
            account_dv="1",
            legal_name="HOUSE TARGARYEN",
        )
        bakery = baker.make(
            Bakery, document_number=document_number, bank_account=bank_account
        )
        return bakery
