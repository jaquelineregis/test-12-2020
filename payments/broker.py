import pagarme

from django.conf import settings


class PagarmeFunctions:
    def __init__(self):
        pagarme.authentication_key(settings.API_KEY)

    def create_recipient(self, params):
        return pagarme.recipient.create(params)

    def create_transaction_split(self, params):
        return pagarme.transaction.create(params)
