from django.db import transaction
from rest_framework import serializers

from customers.models import Customer, AddressShipping
from orders.models import Order, OrderItem
from payments.models import Payment


class CardSerializer(serializers.Serializer):
    number = serializers.CharField()
    cvv = serializers.CharField()
    expiration_date = serializers.CharField()
    holder_name = serializers.CharField()


class BillingSerializer(serializers.Serializer):
    name = serializers.CharField()
    country = serializers.CharField()
    state = serializers.CharField()
    city = serializers.CharField()
    neighborhood = serializers.CharField()
    street = serializers.CharField()
    street_number = serializers.CharField()
    zipcode = serializers.CharField()


class CustomerSerializer(serializers.Serializer):
    name = serializers.CharField()
    type = serializers.CharField()
    country = serializers.CharField()
    email = serializers.CharField()
    type_doc = serializers.CharField()
    number = serializers.CharField()
    phone_numbers = serializers.ListField()
    birthday = serializers.CharField()


class ShippingSerializer(serializers.Serializer):
    name = serializers.CharField()
    fee = serializers.CharField()
    delivery_date = serializers.CharField()
    expedited = serializers.BooleanField()
    country = serializers.CharField()
    state = serializers.CharField()
    city = serializers.CharField()
    neighborhood = serializers.CharField()
    street = serializers.CharField()
    street_number = serializers.CharField()
    zipcode = serializers.CharField()


class ItemSerializer(serializers.Serializer):
    id = serializers.CharField()
    qtd = serializers.CharField()


class PaymentSerializer(serializers.Serializer):
    card = CardSerializer(write_only=True)
    billing = BillingSerializer(write_only=True)
    shipping = ShippingSerializer(write_only=True)
    items = ItemSerializer(many=True, write_only=True)
    customer = CustomerSerializer(write_only=True)

    @transaction.atomic
    def create(self, validated_data):
        customer_data = validated_data.get("customer")
        user_request = self.context["request"].user
        if user_request.is_anonymous:
            customer = Customer.objects.create(**customer_data)
        else:
            customer = Customer.objects.filter(user=user_request).first()
            if not customer:
                customer = Customer.objects.create(**customer_data)

        shipping = validated_data.get("shipping")
        address_shipping, _ = AddressShipping.objects.get_or_create(
            customer=customer, **shipping
        )

        order = Order.objects.create(
            customer=customer,
            address_shipping=address_shipping,
            payment_broker="pagarme",
        )

        items = validated_data.get("items")
        for item in items:
            OrderItem.objects.create(order=order, item_id=item["id"], qtd=item["qtd"])

        card = validated_data.get("card")
        billing = validated_data.get("billing")
        Payment.objects.create(order=order, tmp_card=card, tmp_billing=billing)
        return {"message": "Created payment"}
