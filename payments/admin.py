from django.contrib import admin

from .models import RecipientPagarme, Payment

admin.site.register(RecipientPagarme)
admin.site.register(Payment)
