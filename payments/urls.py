from django.urls import include, path
from rest_framework import routers

from payments.views import PaymentCreateViewSet

api_router = routers.DefaultRouter()
api_router.register(r"payments", PaymentCreateViewSet)

urlpatterns = [
    path("", include(api_router.urls)),
]
