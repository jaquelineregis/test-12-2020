from rest_framework import mixins, viewsets, filters, permissions, authentication

from orders.models import Order, OrderItem
from payments.models import Payment
from payments.serializers import PaymentSerializer


class PaymentCreateViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = [permissions.AllowAny]
    authentication_classes = [authentication.TokenAuthentication]
