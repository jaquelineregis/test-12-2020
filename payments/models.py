import ast

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from bakeries.models import Bakery
from .broker import PagarmeFunctions


class Payment(models.Model):
    order = models.ForeignKey("orders.Order", on_delete=models.CASCADE)
    nm_transaction = models.CharField(max_length=255, blank=True)
    status = models.CharField(max_length=255, blank=True)
    tmp_card = models.TextField(max_length=255, blank=True)
    tmp_billing = models.TextField(max_length=255, blank=True)

    def json_card(self):
        card = self.tmp_card
        return {
            "card_number": card["number"],
            "card_cvv": card["cvv"],
            "card_expiration_date": card["expiration_date"],
            "card_holder_name": card["holder_name"],
        }

    def json_customer(self):
        customer = self.order.customer
        phones = (
            ast.literal_eval(customer.phone_numbers)
            if type(customer.phone_numbers) == str
            else customer.phone_numbers
        )
        return {
            "external_id": str(customer.id),
            "name": customer.name,
            "type": customer.type,
            "country": customer.country,
            "email": customer.email,
            "documents": [{"type": customer.type_doc, "number": customer.number}],
            "phone_numbers": phones,
            "birthday": str(customer.birthday),
        }

    def json_billing(self):
        billing = self.tmp_billing
        return {
            "name": billing["name"],
            "address": {
                "country": billing["country"],
                "state": billing["state"],
                "city": billing["city"],
                "neighborhood": billing["neighborhood"],
                "street": billing["street"],
                "street_number": billing["street_number"],
                "zipcode": billing["zipcode"],
            },
        }

    def json_shipping(self):
        shipping = self.order.address_shipping
        return {
            "name": shipping.name,
            "fee": int(shipping.fee),
            "delivery_date": shipping.delivery_date,
            "expedited": shipping.expedited,
            "address": {
                "country": shipping.country,
                "state": shipping.state,
                "city": shipping.city,
                "neighborhood": shipping.neighborhood,
                "street": shipping.street,
                "street_number": shipping.street_number,
                "zipcode": shipping.zipcode,
            },
        }

    def json_items(self):
        return [
            {
                "id": str(item.item.id),
                "title": item.item.name,
                "unit_price": int(item.item.price * 100),
                "quantity": item.qtd,
                "tangible": item.item.tangible,
            }
            for item in self.order.order_items.all()
        ]

    def json_split_rules(self):
        splits = {}
        for item in self.order.order_items.all():
            recipient_id = item.recipient_id
            splits[recipient_id] = splits.get(recipient_id, 0) + (
                item.item.price * item.qtd
            )

        mkt_perct = settings.MARKETPLACE_PERCENTAGE
        split_rules = [
            {
                "recipient_id": settings.MARKETPLACE_RECIPIENT_ID,
                "amount": int(
                    round(float(self.order.ttl_amount) * (mkt_perct / 100) * 100, 2)
                ),
                "liable": True,
                "charge_processing_fee": True,
            }
        ]
        for key, value in splits.items():
            split_rules.append(
                {
                    "recipient_id": key,
                    "amount": int(round(float(value) * (1 - mkt_perct / 100) * 100, 2)),
                    "liable": False,
                    "charge_processing_fee": False,
                }
            )
        return split_rules


@receiver(post_save, sender=Payment)
def create_payment(sender, instance, created, **kwargs):
    if created:
        amount = instance.order.ttl_amount
        params = {
            "amount": int(amount * 100),
            "customer": instance.json_customer(),
            "billing": instance.json_billing(),
            "shipping": instance.json_shipping(),
            "items": instance.json_items(),
            "split_rules": instance.json_split_rules(),
        }
        params.update(instance.json_card())
        transaction_split = PagarmeFunctions().create_transaction_split(params)

        instance.order.payment_id = transaction_split["id"]
        instance.order.payment_status = transaction_split["status"]
        instance.order.amount = amount
        instance.order.save()

        instance.status = transaction_split["id"]
        instance.nm_transaction = transaction_split["status"]
        instance.tmp_card = ""
        instance.tmp_billing = ""
        instance.save()


class RecipientPagarme(models.Model):
    bakery = models.OneToOneField(
        "bakeries.Bakery", on_delete=models.CASCADE, related_name="recipients"
    )
    recipient_id = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return f"Recipient pagarme nº {self.pk}"

    class Meta:
        verbose_name = "Profile pagarme"
        verbose_name_plural = "Profiles pagarme"

    def json_recipient(self):
        recipient = {
            "anticipatable_volume_percentage": "0",
            "automatic_anticipation_enabled": "false",
            "transfer_day": "27",
            "transfer_enabled": "true",
            "transfer_interval": "monthly",
            "bank_account": self.json_bank_account(),
        }
        return recipient

    def json_bank_account(self):
        bank = self.bakery.bank_account
        bank_account = {
            "agencia": bank.agency,
            "bank_code": bank.bank_code,
            "conta": bank.account,
            "conta_dv": bank.account_dv,
            "document_number": self.bakery.document_number,
            "legal_name": bank.legal_name,
            "type": bank.type,
        }
        if bank.agency_dv:
            bank_account["agencia_dv"] = bank.agency_dv
        return bank_account


@receiver(post_save, sender=Bakery)
def create_obj_recipient_pagarme(sender, instance, created, **kwargs):
    if created:
        RecipientPagarme.objects.create(bakery=instance)


@receiver(post_save, sender=RecipientPagarme)
def create_recipient_in_pagarme(sender, instance, created, **kwargs):
    if created:
        recipient = PagarmeFunctions().create_recipient(instance.json_recipient())
        instance.recipient_id = recipient["id"]
        instance.save()
