#!/bin/sh

echo "Remove migrations and sqlite3"
find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*.sqlite3" -delete

echo "Apply database migrations"
python manage.py makemigrations bakeries orders customers payments
python manage.py migrate

echo "Adding fixtures"
python manage.py loaddata fixtures/db.json

echo "Starting server"
python manage.py runserver
