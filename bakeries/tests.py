import json
from unittest.mock import patch
from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from model_bakery import baker
from rest_framework.authtoken.models import Token

from .models import Bakery, Item, BankAccount


@patch("payments.broker.PagarmeFunctions.create_recipient", {"id": "re_123"})
class UnitTest(TestCase):
    def setUp(self):
        user = baker.make(User, is_superuser=True)
        token, created = Token.objects.get_or_create(user=user)
        self.client_authenticated = Client(HTTP_AUTHORIZATION=f"Token {token.key}")

    def test_list_bakery_return_unauthorized_without_token(self):
        response = self.client.get(reverse("bakery-list"))
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}',
        )

    def test_list_bakery_return_authorized_with_token(self):
        response = self.client_authenticated.get(reverse("bakery-list"))
        self.assertEqual(response.status_code, 200)

    def test_create_bakery_return_unauthorized_without_token(self):
        response = self.client.post(reverse("bakery-list"), data={})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}',
        )

    def test_create_bakery_return_201(self):
        data = {
            "user": {"password": "123456"},
            "fantasy_name": "Padaria do Bessa 2",
            "document_number": "70.025.343/0001-96",
            "bank_account": {
                "agency": "0932",
                "bank_code": "341",
                "account": "58054",
                "account_dv": "1",
                "legal_name": "HOUSE TARGARYEN",
            },
        }
        response = self.client_authenticated.post(
            reverse("bakery-list"), json.dumps(data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 201)
        bakery = Bakery.objects.get()
        self.assertEqual(bakery.document_number, "70025343000196")
        self.assertEqual(bakery.bank_account.agency, "0932")

    def test_list_item_return_authorized_without_token(self):
        response = self.client.get(reverse("item-list"))
        self.assertEqual(response.status_code, 200)

    def test_create_item_return_authorized_without_token(self):
        response = self.client.post(reverse("item-list"), data={})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(
            response.content,
            b'{"detail":"Authentication credentials were not provided."}',
        )

    def test_create_item_return_201(self):
        bakery = self.md_bakery()
        data = {"name": "Panetone", "price": "7.90", "stock": "10", "bakery": bakery.pk}
        response = self.client_authenticated.post(
            reverse("item-list"), json.dumps(data), content_type="application/json"
        )
        self.assertEqual(response.status_code, 201)
        item = Item.objects.get()
        self.assertEqual(item.name, "Panetone")

    def test_list_item_filter_by_name(self):
        bakery = self.md_bakery()
        item = baker.make(Item, bakery=bakery, name="Bolacha")
        item2 = baker.make(Item, bakery=bakery, name="Biscoito")

        searched = "Biscoito"
        response = self.client_authenticated.get(
            f"{reverse('item-list')}?search={searched}"
        )
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["name"], searched)

    def test_list_item_filter_by_price(self):
        bakery = self.md_bakery()
        item = baker.make(Item, bakery=bakery, price=1.11)
        item2 = baker.make(Item, bakery=bakery, price=2.21)

        searched = 2.21
        response = self.client_authenticated.get(
            f"{reverse('item-list')}?search={searched}"
        )
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()[0]["price"], "%.2f" % searched)

    def test_list_item_filter_by_price_but_not_found(self):
        bakery = self.md_bakery()
        item = baker.make(Item, bakery=bakery, price=1.11)

        searched = 3.33
        response = self.client_authenticated.get(
            f"{reverse('item-list')}?search={searched}"
        )
        self.assertEqual(len(response.json()), 0)

    def md_bakery(self):
        bank_account = baker.make(
            BankAccount,
            agency="0932",
            bank_code="341",
            account="58054",
            account_dv="1",
            legal_name="HOUSE TARGARYEN",
        )
        bakery = baker.make(
            Bakery, document_number="70025343000196", bank_account=bank_account
        )
        return bakery
