from django.urls import include, path
from rest_framework import routers

from bakeries.views import BakeryViewSet, ItemViewSet

api_router = routers.DefaultRouter()
api_router.register(r"bakeries", BakeryViewSet)
api_router.register(r"items", ItemViewSet)

urlpatterns = [
    path("", include(api_router.urls)),
]
