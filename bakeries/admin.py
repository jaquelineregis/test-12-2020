from django.contrib import admin

from . import models


class ItemInline(admin.TabularInline):
    model = models.Item
    extra = 1


class BakeryAdmin(admin.ModelAdmin):
    inlines = [ItemInline]


admin.site.register(models.Bakery, BakeryAdmin)
admin.site.register(models.BankAccount)
admin.site.register(models.Item)
