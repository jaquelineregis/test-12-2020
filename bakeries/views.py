from rest_framework import viewsets, filters, permissions, authentication

from bakeries.models import Bakery, Item
from bakeries.serializers import BakerySerializer, ItemSerializer


class BakeryViewSet(viewsets.ModelViewSet):
    queryset = Bakery.objects.all()
    serializer_class = BakerySerializer
    permission_classes = [permissions.DjangoModelPermissions]
    authentication_classes = [authentication.TokenAuthentication]


class ItemViewSet(viewsets.ModelViewSet):
    queryset = Item.objects.all()
    serializer_class = ItemSerializer
    permission_classes = [permissions.DjangoModelPermissionsOrAnonReadOnly]
    authentication_classes = [authentication.TokenAuthentication]
    search_fields = (
        "name",
        "price",
    )
    filter_backends = (filters.SearchFilter,)
