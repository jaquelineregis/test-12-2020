from django.contrib.auth.models import User
from django.db import models

from bakeries.validators import (
    validate_is_alpha,
    validate_is_digit,
    validate_is_cpf_or_cnpj,
)


class BankAccount(models.Model):
    agency = models.CharField(max_length=5, validators=[validate_is_digit])
    agency_dv = models.CharField(
        max_length=1, blank=True, validators=[validate_is_digit]
    )
    bank_code = models.CharField(max_length=3, validators=[validate_is_digit])
    account = models.CharField(max_length=13, validators=[validate_is_digit])
    account_dv = models.CharField(max_length=2)
    legal_name = models.CharField(max_length=30, validators=[validate_is_alpha])
    type = models.CharField(
        default="conta_corrente",
        max_length=25,
        choices=(
            ("conta_corrente", "Conta corrente"),
            ("conta_poupanca", "Conta poupança"),
            ("conta_corrente_conjunta", "Conta corrente conjunta"),
            ("conta_poupanca_conjunta", "Conta poupança conjunta"),
        ),
    )

    def __str__(self):
        return str(self.pk)


class Bakery(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    fantasy_name = models.CharField(max_length=255)
    document_number = models.CharField(
        max_length=18, unique=True, validators=[validate_is_cpf_or_cnpj]
    )
    bank_account = models.OneToOneField(
        "bakeries.BankAccount", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.fantasy_name

    class Meta:
        verbose_name = "Bakery"
        verbose_name_plural = "Bakeries"


class Item(models.Model):
    bakery = models.ForeignKey(
        "bakeries.Bakery", on_delete=models.CASCADE, related_name="items"
    )
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=15, decimal_places=2)
    stock = models.PositiveIntegerField()
    tangible = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.name} in {self.bakery}"
