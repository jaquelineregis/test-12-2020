import re

from django.contrib.auth.models import User
from django.db import IntegrityError, transaction
from rest_framework import serializers

from bakeries.models import Bakery, Item, BankAccount


class BankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = [
            "id",
            "agency",
            "agency_dv",
            "bank_code",
            "account",
            "account_dv",
            "legal_name",
            "type",
        ]


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["email", "password"]


class ItemSerializer(serializers.ModelSerializer):
    bakery__fantasy_name = serializers.ReadOnlyField(source="bakery.fantasy_name")

    class Meta:
        model = Item
        fields = ["id", "name", "price", "stock", "bakery", "bakery__fantasy_name"]


class BakerySerializer(serializers.ModelSerializer):
    user = UserSerializer(write_only=True)
    bank_account = BankAccountSerializer(write_only=True)
    items = ItemSerializer(many=True, read_only=True)

    class Meta:
        model = Bakery
        fields = [
            "id",
            "user",
            "fantasy_name",
            "document_number",
            "bank_account",
            "items",
        ]

    def validate_document_number(self, data):
        return re.sub(r"\D+", "", data)

    @transaction.atomic
    def create(self, validated_data):
        try:
            user_data = validated_data.pop("user")
            user = User.objects.create_user(
                username=validated_data.get("document_number"), **user_data
            )
        except IntegrityError as ex:
            raise serializers.ValidationError(
                {"document_number": ["Bakery with document number already exist."]}
            )

        bank_account_data = validated_data.pop("bank_account")
        bank_account = BankAccount.objects.create(**bank_account_data)

        instance = Bakery.objects.create(
            user=user, bank_account=bank_account, **validated_data
        )
        return instance
