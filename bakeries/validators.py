import re

from django.core.exceptions import ValidationError


def validate_is_digit(value):
    if not value.isdigit():
        raise ValidationError(
            "%(value)s has a non-numeric character",
            params={"value": value},
        )


def validate_is_alpha(value):
    if not value.replace(" ", "").isalpha():
        raise ValidationError(
            "%(value)s has a non-alpha character",
            params={"value": value},
        )


def validate_is_cpf_or_cnpj(value):
    numbers = re.sub(r"\D+", "", value)
    if len(numbers) not in [11, 14]:
        raise ValidationError(
            "Document, %(value)s, is missing numbers.",
            params={"value": value},
        )
