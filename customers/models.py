from django.contrib.auth.models import User
from django.db import models


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=255)
    email = models.EmailField()
    type = models.CharField(max_length=18)
    type_doc = models.CharField(max_length=18)
    number = models.CharField(max_length=18)
    phone_numbers = models.CharField(
        max_length=255,
        default=" ['+5511999773311']",
        help_text="Adicione o numero nesse formato: ['+5511999773311']",
    )
    country = models.CharField(max_length=18)
    birthday = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.name


class AddressShipping(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    fee = models.PositiveIntegerField(default=0)
    delivery_date = models.DateField()
    expedited = models.BooleanField()
    country = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    neighborhood = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
    street_number = models.CharField(max_length=255)
    zipcode = models.CharField(max_length=255)

    def __str__(self):
        return self.zipcode
