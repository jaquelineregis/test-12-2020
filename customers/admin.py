from django.contrib import admin

from . import models


class AddressShippingInline(admin.TabularInline):
    model = models.AddressShipping
    extra = 1


class CustomerAdmin(admin.ModelAdmin):
    inlines = [AddressShippingInline]


admin.site.register(models.Customer, CustomerAdmin)
admin.site.register(models.AddressShipping)
